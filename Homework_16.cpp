﻿#include <ctime>
#include <iostream>

using namespace std;

int Date()
{
    struct tm now;
    time_t t = time(0);
    localtime_s(&now, &t);
    return now.tm_mday;
}

int main()
{
    const int N=5;
    int array[N][N];
    int D = Date();
    int sum = 0;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            cout << array[i][j] << " ";
            
            if (i == D % N)
            {
                sum += array[i][j];
            }
        }
        cout << "\n";
    }
    cout << "The sum of the values in row " << D % N + 1 << ": " << sum;
      
}
